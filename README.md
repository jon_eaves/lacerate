lacerate
--------

A tool to check for local bleeding.

This is a very simple shell script with a couple of dependencies that allows users of 1Password to check if servers that they are using have patched OpenSSL for the Heartbleed bug. It was developed on MacOS/X, so no guarantees that it will work anywhere else.

I wrote this because I use 1Password, have a lot of entries and wanted to know when I could safely (well, weird word these days) go and change my passwords on the servers. 

I cannot be certain that this will work properly for you, because the database export functionality in 1Password appears to be pretty awful, so yell at them for creating terrible files. I use 1Password 4 for Mac and Windows, and was able to work with the exported pif files from both.

I'll also note that I'm not 100% certain that I've got all the sites, nor have I not missed things that look important to you. Feel free to hack the living daylights out of the script, you have my permission. I do a couple of things to remove entries from the list that look like they are not useful to me. For example, I remove all entries that don't have a period (.) in them - because I don't want to find a smarter way to filter out chrome:// or http://localhost entries that have crept into my now rather bloated 1Password database.

This has turned terribly LiveJournal, but basically use this as you see fit - don't yell at me if it breaks, I'll try and help you if you act like a rational person.

Things you will need;

1. Heartbleed binary from https://github.com/FiloSottile/Heartbleed
2. jq (JSON parsing) from http://stedolan.github.io/jq/
3. Some form of Unix that will run /bin/sh that's vaguely compatible with MacOS/X

Things you need to do:

export HB_DIR=directory containing Heartbleed binary

Tool Use: 

lacerate file.pif [noexec]

Where file.pif is the name of the file containing the entries. If you export from a Mac, it creates some whacky sub-directory structure - the Windows version just created a file.

Optionally you can add a second parameter (it can be anything, I use 'foo') and it will not execute the Heartbleed check, but just print out the urls.

One final thing that is useful - anything that isn't the actual output of the script execution is sent to stderr. This means running it in interactive mode you get a little bit of information, but if you want to capture the output and process it later (I can see this being useful as a means to just process the 1Password file, and then do what you like with the URLs) then just redirect stderr to /dev/null

lacerate myfile.pif foo 2>/dev/null


